### Entendendo o problema
O pagador de promessas *"negociou"* com **3** igrejas a fator de multiplicação do seu dinheiro de **2x**, doando **20** reais a cada negociação e saiu, no final com **0** reais.
Ou seja, a cada igreja, seu dinheiro inicial **x** é multiplicado por 2 (**f**ator) e subtraído o valor da **d**oação, ficando com um valor final **y**.

Em fórmula matemática, para o caso específico do nosso pagador de promessas temos:
> **2x - 20 = 0**

Generalizando:
> **fx - d = y** , onde:  
> **f** = fator de multiplicação negociado  
> **x** = valor inicial com o qual chega em cada igreja  
> **d** = valor da doação negociada  
> **y** = valor final que ele fica após cumprir a promessa  
>> **y** vira valor inicial **x** ao ir e negociar com outra igreja.

No problema queremos saber qual foi o **valor inicial x** com o qual o pagador de promessas chegou na igreja.
Por isso, colocaremos **x** em evidência, ficando:
> **x = (y + d) / f**

### Colocando em linguagem do shell
Como a formula acima serve para determinar o valor inicial x de ***apenas uma "negociação"***, ele deve ser aplicada **n** *vezes quantas forem as igrejas com as quais o pagador de promessas negociou*. Assim, implementei no [script](https://codeberg.org/eppursimuove/csp2022/src/commit/cb2938c56a1254f86515c7ab6cb256ce1979d994/a01/pagador.sh) que a formula fosse resolvida **n** vezes.

### Testando com diversos valores
O [script](https://codeberg.org/eppursimuove/csp2022/src/commit/cb2938c56a1254f86515c7ab6cb256ce1979d994/a01/pagador.sh) pode ser executado sem argumento algum, dessa forma ele faz os cálculos baseados no exemplo específico do desafio.
Mas também podem ser definidos os argumentos: 

**./pagador.sh \[-v=valor_final] \[-n=nr_igrejas] \[-d=doação] \[-f=fator]**

Todos os argumentos são opcionais, os que não forem explicitados adotarão os valores do desafio.