#!/usr/bin/env bash
# -----------------------------------------------------
# Nome		: pagador.sh
# Descrição	: desafio do pagador de promessas
# Versão	: 0.1-beta
# Autor		: Eppur Si Muove
# Contato	: eppur.si.muove@keemail.me
# Criação	: 07/01/2022
# Modificação:
# Licença	: GNU/GPL v3.0
# CDNEditor	: vim
# -----------------------------------------------------
# set -x
# Geral : fx - d = y | ou | x = (y + d) / f
#     f = fator de multiplicação
#     x = valor inicial
#     d = valor da doação
#     y = valor final (vira o valor inicial - x - para transação com outro santo)
#
# ./pagador.sh [-v=valor_final] [-n=nr_igrejas] [-d=doação] [-f=fator]

while [ -n "$1" ]; do
	case $1 in
		-v=*) VALOR_FINAL=$(cut -d= -f2 <<< ${1//,/.}) ;;
		-n=*) NR_IGREJAS=$(cut -d= -f2 <<< $1) ;;
		-d=*) DOACAO=$(cut -d= -f2 <<< ${1//,/.}) ;;
		-f=*) FATOR=$(cut -d= -f2 <<< ${1//,/.}) ;;
		*) echo Argumento inválido: $1; exit 1 ;;
	esac
	shift
done

VALOR_INICIAL=
VALOR_FINAL=${VALOR_FINAL:-0}
NR_IGREJAS=${NR_IGREJAS:-3}
DOACAO=${DOACAO:-20}
FATOR=${FATOR:-2}

# copia e cola da função do Blau Araújo
_calc(){ awk 'BEGIN { printf "%.'${2:-2}'f\n", '"$1"' }'; }

# x = (y + d) / f
_valor_inicial(){ VALOR_INICIAL=$( _calc "($1 + $DOACAO) / $FATOR" ); }

i=1
VF=$VALOR_FINAL
while (( i <= $NR_IGREJAS )); do
	_valor_inicial $VALOR_FINAL
	VALOR_FINAL=$VALOR_INICIAL
	i=$((++i))
done

echo Se o pagador de promessas \"negociou\" com $NR_IGREJAS igrejas à um fator de multiplicação de ${FATOR}x, doando R\$ ${DOACAO//./,} e ficou, no final, com R\$ ${VF//./,}, então ele chegou com R\$ ${VALOR_INICIAL//./,} na primeira igreja.
